//Program to create an application level file sharing protocol 

//Header Files
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include "stdlib.h"
#include <unistd.h>
#include <openssl/md5.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <regex.h>
#include <time.h>

//Function Prototypes
void tcp_client(int port, char ip[20]);
void tcp_server(char port[20]);

void udp_client(int port, char ip[20]);
void udp_server(char port[20]);
int match(const char *string, char *pattern);
void IndexGet(char command[1024],int connectionSocket);
void FileHash(char command[1024],int connectionSocket);

char filename_global[1024]={'\0'};

//Main Function
int main(int argc, char *argv[])
{
	int pid, i=0;

	int client_port, flag = -1;
	char client_ip[20], protocol[10], server_port[20];

	printf("Server Port: ");
	scanf("%s", server_port);

	printf("Client Port: ");
	scanf("%d", &client_port);

	printf("Client IP: ");
	scanf("%s", client_ip);

	printf("Protocol (TCP/UDP): ");
	scanf("%s", protocol);


	while(protocol[i]!= '\0'){
		protocol[i] = toupper(protocol[i]);
		i++;
	}

	if(strcmp(protocol, "TCP") == 0){
		flag = 1;
	}
	else if (strcmp(protocol, "UDP") == 0){
		flag = 0;
	}
	else {
		printf("Invalid Protocol Entry. Run Program Again\n");
		return 0;
	}

	//To run the client and server paralelly as parent and child processes
	pid = fork();

	if(pid == 0){
		if(flag)
			tcp_client(client_port, client_ip);
		//else
		//	udp_client(client_port, client_ip);
	}
	else{
		if(flag)
			tcp_server(server_port);
		//else
		//	udp_server(server_port);
	}

	return 0;

}

//Client Function
void tcp_client(int client_port, char client_ip[20]){

	int ClientSocket = 0;
	struct sockaddr_in serv_addr;

	//Creation Of Socket
	ClientSocket = socket(AF_INET,SOCK_STREAM,0);

	if(ClientSocket<0)
	{
		printf("ERROR WHILE CREATING A SOCKET\n");
		return;
	}
	else
	{
		printf("[CLIENT] Socket created \n");				
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(client_port);
	
	//Checking For Valid IP Addresses and seeting serv.addr.sin_addr
	if(inet_pton(AF_INET, client_ip, &serv_addr.sin_addr) != 1)
    {
        printf("Invalid IP Address Entered\n");
        return;
    }

	//Connection to Server
	while(connect(ClientSocket,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0);
	printf("[CLIENT] Connection Established");

	char buffer[1024]={'\0'};
	char temp[1024]={'\0'};
	char file[1024]={'\0'};
	char * split;	
	unsigned long len;
	int n;

	while(1){

		bzero(temp, 1024);
			

		printf("\nCommand_client - ");
		char c = getchar();
		fgets(buffer, 1024, stdin);

		bzero(temp, 1024);


		//Making a copy of the command since it is needed later and will get modified now
		strcpy(temp, buffer);

		if(!strncmp(temp, "quit", 4) || !strncmp(temp, "Quit", 4) ){
			printf("Connection Terminated\n");
			close(ClientSocket);
			exit(0);
		}

		//Splitting based on space & newline
		split = strtok(temp, " \n");

		char file_read[1024]={'\0'};

		if(!strcmp(split, "IndexGet")){
			if(send(ClientSocket,buffer,strlen(buffer)-1,0)<0)
				printf("ERROR while writing to the socket\n");
			int n;

			while(1){
				n = read(ClientSocket, file_read, sizeof(buffer)-1);

				if(n<=0){
					break;
				}

				printf("%s\n", file_read);
				bzero(file_read, sizeof(file_read));

			}


		}

		if(!strcmp(split, "FileUpload")){
			if(send(ClientSocket,buffer,strlen(buffer)-1,0)<0)
				printf("ERROR while writing to the socket\n");
			
			split = strtok(NULL, " \n");
			char filename[1024]={'\0'};
			
			strcpy(filename, split);
			printf("%s  ", filename);
			
        	  	
			write(ClientSocket,buffer,strlen(buffer));
			memset(buffer, 0, sizeof(buffer));
			n = read(ClientSocket, buffer, sizeof(buffer)-1);

			if(strcmp(buffer,"FileUploadAllow") == 0){
				printf("File Upload Allowed\n");
				
				FILE *fp = fopen(filename, "rb");
				char file_read[1024]={'\0'};

				while(!feof(fp)){
					int size = fread(file_read, 1, 1024, fp);
					send(ClientSocket,file_read,strlen(file_read),0);
					bzero(file_read, 1024);
				}

				memcpy(file_read, "@#@#", 4);
				send(ClientSocket,file_read,4,0);
				bzero(file_read, 1024);
				fclose(fp);

			}
			else if(strcmp(buffer,"FileUploadDeny") == 0)
		  	{
			    printf("File Upload Denied\n");
			    memset(buffer, 0,sizeof(buffer));
			    continue;
			}

		}

		if(!strcmp(split, "FileHash")){
			if(send(ClientSocket,buffer,strlen(buffer)-1,0)<0)
				printf("ERROR while writing to the socket\n");
			int n;

			while(1){
				n = read(ClientSocket, file_read, sizeof(buffer)-1);

				if(n<=0){
					break;
				}

				printf("%s\n", file_read);
				bzero(file_read, sizeof(file_read));

			}


		}

		if(!strcmp(split, "FileDownload")) {
			split = strtok(NULL, " \n");
			char filename[1024]={'\0'};
			strcpy(filename, split); 

			//Creating a File & Writing in Binary
			FILE *fp = fopen(filename, "wb");

			if(send(ClientSocket,buffer,strlen(buffer)-1,0)<0)
				printf("ERROR while writing to the socket\n");

			//bzero(buffer,1024);			

			//if(recv(ClientSocket,buffer,1024,0)<0)
			//	printf("ERROR while reading from the socket\n");

			//To mark the starting to the file
			//if(buffer[0]=='@' && buffer[1]=='#' && buffer[2]=='@' && buffer[3]=='#'){
				
				bzero(buffer, 1024);
				int n;
				while(1){
					n = read(ClientSocket, buffer, sizeof(buffer)-1);
					printf("%s\n", buffer );

					//To mark the ending of the file
					if(buffer[n-1]=='#' && buffer[n-2]=='@' && buffer[n-3]=='#' && buffer[n-4]=='@'){
						
						fwrite(buffer,1,n-4,fp);						
						fclose(fp);
						printf("File SUCCESSFULLY Transferred!");
						bzero(buffer, 1024);
						break;
					}
					else{
						fwrite(buffer,1,n,fp);
						bzero(buffer, 1024);	
					}

				}

			//}
			//else{
			//	printf("%s", buffer);
			//	bzero(buffer, 1024);
			//	continue;
			//}

		}
		//else if(!send_buffer)
			
	}


	/*
	printf("\nEnter Your Message: ");
	scanf("%s",buffer);

	if(send(ClientSocket,buffer,strlen(buffer),0)<0)
		printf("ERROR while writing to the socket\n");

	bzero(buffer,1024);

	//if(recv(ClientSocket,buffer,1024,0)<0)
	//	printf("ERROR while reading from the socket\n");

	//printf("\nMessage from Server: ");
	//printf("%s\n",buffer );

	printf("Closing Connection\n");
	//close(ClientSocket);
	*/
		
	
}



void tcp_server(char server_port[20]){

	int listenSocket = 0, connectionSocket = 0;
	struct sockaddr_in serv_addr;			


	//Formation of the Socket
	listenSocket = socket(AF_INET,SOCK_STREAM,0);
	if(listenSocket<0)
	{
		printf("ERROR WHILE CREATING A SOCKET\n");
		return;
	}
	else
		printf("[SERVER] SOCKET ESTABLISHED SUCCESSFULLY\n\n");


	bzero((char *) &serv_addr,sizeof(serv_addr));
	int port = atoi(server_port);
	serv_addr.sin_family = AF_INET;	
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(port);


	//Binding the socket to the ip & port
	if(bind(listenSocket,(struct sockaddr * )&serv_addr,sizeof(serv_addr)) == -1)
		printf("ERROR WHILE BINDING THE SOCKET\n");
	else
		printf("[SERVER] SOCKET BINDED SUCCESSFULLY\n");


	//Listening for connections
	if(listen(listenSocket,10) == -1)	
		printf("[SERVER] FAILED TO ESTABLISH LISTENING \n");
	
	printf("[SERVER] Waiting for client to connect....\n" );


	//Accepting Connections on this port
	while((connectionSocket=accept(listenSocket , (struct sockaddr*)NULL,NULL))<0);
	printf("[CONNECTED]\n");


	char buffer[1024]={'\0'};
	char send_buffer[1024]={'\0'};
	char file_read[1024]={'\0'};
	char temp[1024]={'\0'};
	char *split;
	int n, size;

	n=read(connectionSocket, buffer, sizeof(buffer));

	while(n>0){
		
		//printf("Received - %s", buffer);
		bzero(temp, 1024);
/*		temp[0]='\0';
		send_buffer[0]='\0';
		file_read[0]='\0';
		buffer[0]='\0';	
*/

		send_buffer[0]='\0';
		file_read[0]='\0';
		
		strcpy(temp, buffer);

		char buffer2[1024]={'\0'};
		strcpy(buffer2,buffer);

		split = strtok(temp, " \n");

		if(!strcmp(split, "FileDownload")){
			split = strtok(NULL, " \n");
			FILE *fp = NULL;
			char filename[1024] = {'\0'};
			strcpy(filename, split);

			fp = fopen(filename, "rb");

			split = strtok(NULL, " \n");
			if(split != NULL){
				sprintf(send_buffer, "Incorrect Format. Correct Format is - FileDownload <filename> \n");
				send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
				bzero(send_buffer, 1024);
			}

			//memcpy(send_buffer, "@#@#", 4);
			//send(connectionSocket,send_buffer,4,0);
			//bzero(send_buffer, 1024);	
			//bzero(file_read, 1024);		

			while(!feof(fp)){

				//Splitting the file in units of 1024
				size = fread(file_read, 1, 1024, fp);

				//Since the size read may not be 1024, we need to send only what is read
				//Or else garbage char. will get sent
				memcpy(send_buffer, file_read, size);
				send(connectionSocket,send_buffer,strlen(send_buffer),0);
				bzero(file_read, 1024);
				bzero(send_buffer, 1024);
			}

			//To Mark the end of the file 
			memcpy(send_buffer, "@#@#", 4);
			send(connectionSocket,send_buffer,4,0);
			bzero(send_buffer, 1024);
			fclose(fp);
		
		}
		else if(!strcmp(split, "FileUpload")){
			
			split = strtok(NULL, " \n");
			char filename[1024]={'\0'};
			strcpy(filename, split); 
			
			//Creating a File & Writing in Binary

			FILE *fp = NULL;
			fp = fopen(filename, "wb");

			if(!fp)
				send(connectionSocket,"FileUploadDeny",14,0);
			else
			{
				send(connectionSocket,"FileUploadAllow",15,0);

				bzero(file_read, 1024);
				int n;
				while(1){
					n = read(connectionSocket, file_read, sizeof(file_read)-1);
					
					//To mark the ending of the file
					if(file_read[n-1]=='#' && file_read[n-2]=='@' && file_read[n-3]=='#' && file_read[n-4]=='@'){
						
						fwrite(file_read,1,n-4,fp);						
						fclose(fp);
                        			bzero(file_read, 1024);
                       				break;
					}
					else{
						fwrite(file_read,1,n,fp);
						bzero(file_read, 1024);	
					}

				}

			}

		}
		else if(!strcmp(split, "IndexGet")){
			IndexGet(buffer2,connectionSocket);
		}
		else if(!strcmp(split, "FileHash")){
			FileHash(buffer2,connectionSocket);
		}
		
		bzero(send_buffer, 1024);
		bzero(buffer, 1024);
		while((n = read(connectionSocket,buffer,sizeof(buffer)))<=0);




	}


	/*

	char buffer[1024];
	bzero(buffer,1024);
	if(recv(connectionSocket,buffer,1024,0)<0)
		printf("ERROR whiile reading from Client\n");
	printf("Message from Client: %s\n",buffer );
	bzero(buffer,1024);

	//close(connectionSocket);
	//close(listenSocket);

	*/
	wait(NULL);
}



void udp_server(char server_port[20]){

	int listenSocket = 0;	 

	int connectionSocket = 0;

	struct sockaddr_in serv_addr;			

	int port = atoi(server_port);

	listenSocket = socket(AF_INET,SOCK_STREAM,0);
	if(listenSocket<0)
	{
		printf("ERROR WHILE CREATING A SOCKET\n");
		return;
	}
	else
		printf("[SERVER] SOCKET ESTABLISHED SUCCESSFULLY\n\n");

	bzero((char *) &serv_addr,sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;	
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(port);

	if(bind(listenSocket,(struct sockaddr * )&serv_addr,sizeof(serv_addr))<0)
		printf("ERROR WHILE BINDING THE SOCKET\n");
	else
		printf("[SERVER] SOCKET BINDED SUCCESSFULLY\n");

	if(listen(listenSocket,10) == -1)	
	{
		printf("[SERVER] FAILED TO ESTABLISH LISTENING \n\n");
	}
	printf("[SERVER] Waiting fo client to connect....\n" );

	while((connectionSocket=accept(listenSocket , (struct sockaddr*)NULL,NULL))<0);

	printf("[CONNECTED]\n");

	char buffer[1024];
	bzero(buffer,1024);
	if(recv(connectionSocket,buffer,1024,0)<0)
		printf("ERROR whiile reading from Client\n");
	printf("Message from Client: %s\n",buffer );
	bzero(buffer,1024);

	//close(connectionSocket);
	//close(listenSocket);
}

//Client Function
void udp_client(int client_port, char client_ip[20]){

	int ClientSocket = 0;
	struct sockaddr_in serv_addr;
	char buffer[1024];

	bzero(buffer,1024);

	ClientSocket = socket(AF_INET,SOCK_STREAM,0);

	if(ClientSocket<0)
	{
		printf("ERROR WHILE CREATING A SOCKET\n");
		return;
	}
	else
	{
		printf("[CLIENT] Socket created \n");				
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(client_port);
	serv_addr.sin_addr.s_addr = inet_addr(client_ip);

	while(connect(ClientSocket,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0);

	printf("\nEnter Your Message: ");
	scanf("%s",buffer);

	if(send(ClientSocket,buffer,strlen(buffer),0)<0)
		printf("ERROR while writing to the socket\n");

	bzero(buffer,1024);

	//if(recv(ClientSocket,buffer,1024,0)<0)
	//	printf("ERROR while reading from the socket\n");

	//printf("\nMessage from Server: ");
	//printf("%s\n",buffer );

	printf("Closing Connection\n");
	//close(ClientSocket);

}

void FileHash(char command[1024], int connectionSocket)
{
    char *split_cmnd;
    char send_buffer[1024]={'\0'};
    split_cmnd = strtok(command, " \n");
    split_cmnd = strtok(NULL, " \n");
    if(split_cmnd != NULL)
    {
        if(!strcmp(split_cmnd,"--verify"))
        {
            split_cmnd = strtok(NULL, " \n");
            if(split_cmnd != NULL)
            {
                DIR *dr;
                struct dirent *drnt;
                dr = opendir("./");
                struct stat filestat;
                //char *filename;
                char *filename = split_cmnd;
                time_t timestamp;
                unsigned char md[MD5_DIGEST_LENGTH];
                //size_t result;
                unsigned char buffer[1024];
                int bytes;
                if(dr != NULL)
                {
                    while( drnt = readdir(dr))
                    {
                        if(stat(drnt->d_name,&filestat) < 0)
                            return;
                        else if(!strcmp(filename,drnt->d_name))
                        {
                            filename = drnt->d_name;
                            timestamp = filestat.st_mtime;
                            MD5_CTX md5_fd;
                            FILE *fp;
                            fp = fopen(filename, "r");
                            if(fp == NULL)
                            {
                                sprintf(send_buffer, "The Requested File cannot be opened.\n");
								send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
								bzero(send_buffer, 1024);
                                return;
                            }
                           
                            MD5_Init(&md5_fd);
                            while ((bytes = fread (buffer, 1, 1024, fp)) != 0)
                                MD5_Update (&md5_fd, buffer, bytes);
                            MD5_Final(md,&md5_fd);
                            fclose(fp);
                        
                        }
                    }
                    closedir(dr);
                    sprintf(send_buffer, "Filename: %s      Timestamp: %d      Checksum: %u\n",filename,timestamp,md);
					send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
					bzero(send_buffer, 1024);
                    //printf("filename: %s    timestamp: %d    hashedfile:  %u\n",filename,timestamp,md);
                }
                else
                {
                    //printf("Directory could not be opened.\n");
                    sprintf(send_buffer, "Directory cannot be opened.\n");
					send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
					bzero(send_buffer, 1024);
                }
            }
            else if(split_cmnd == NULL)
            {
                //printf("Incorrect format used\n");
                sprintf(send_buffer, "Incorrect Format. The Correct Format is FileHash --verify <filename>.\n");
				send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
				bzero(send_buffer, 1024);
                return;
            }
            split_cmnd = strtok(NULL, " \n");
            if(split_cmnd != NULL)
            {
                //printf("Incorrect format followed.\n");
                sprintf(send_buffer, "Incorrect Format. The Correct Format is FileHash --verify <filename>.\n");
				send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
				bzero(send_buffer, 1024);
                return;
            }
        }
        else if(!strcmp(split_cmnd,"--checkall"))
        {
            split_cmnd = strtok(NULL, " \n");
            if(split_cmnd == NULL)
            {
                DIR *dr;
                struct dirent *drnt;
                dr = opendir("./");
                struct stat filestat;
                char *filename;
                time_t timestamp;
                unsigned char md[MD5_DIGEST_LENGTH];
                unsigned char buffer[1024];
                size_t result;
                int bytes;
                if(dr != NULL)
                {
                    while( drnt = readdir(dr))
                    {
                        if(stat(drnt->d_name,&filestat) < 0)
                            return;
                        else
                        {
                            filename = drnt->d_name;
                            timestamp = filestat.st_mtime;
                            MD5_CTX md5_fd;
                            FILE *fp;
                            fp = fopen(filename, "r");
                            if(fp == NULL)
                            {
                                //printf("File couldn't be opened\n");
                                sprintf(send_buffer, "File cannot be opened.\n");
								send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
								bzero(send_buffer, 1024);
                                // return 0;
                            }
                            else
                            {
                            	MD5_Init(&md5_fd);
                            	while ((bytes = fread (buffer, 1, 1024, fp)) != 0)
                             	   MD5_Update (&md5_fd, buffer, bytes);
                            	MD5_Final(md,&md5_fd);

                            	fclose(fp);
                            	//printf("filename: %s    timestamp: %d    hashedfile:  %u\n",filename,timestamp,md);
                            	sprintf(send_buffer, "Filename: %s     Timestamp: %d     Checksum:  %u\n",filename,timestamp,md);
								send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
								bzero(send_buffer, 1024);
                            //}
                            }
                        }
                    }
                    //closedir(dr);
                }
                else
                {
                    //printf("Directory could not be opened.\n");
                    sprintf(send_buffer, "Directory cannot be opened.\n");
					send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
					bzero(send_buffer, 1024);
					return;
                }
            }
            else
            {
                //printf("Incorrect format followed.\n");
                sprintf(send_buffer, "Incorrect Format. The Correct Format is FileHash --checkall\n");
				send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
				bzero(send_buffer, 1024);
				return;
            }
        }
    }
    else
    {
        printf("Please use correct format\n");
        sprintf(send_buffer, "Incorrect Format. The Correct Format is FileHash --checkall\n");
		send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
		bzero(send_buffer, 1024);
		return;
    }
}

int match(const char *string, char *pattern)
{
    int status;
    regex_t re;

    if (regcomp(&re, pattern, REG_EXTENDED|REG_NOSUB) != 0) {
        return 0;      
    }
    status = regexec(&re, string, (size_t) 0, NULL, 0);
    regfree(&re);
    if (status != 0) 
    {
        return 0;     
    }
    return 1;
}

void IndexGet(char command[1024], int connectionSocket)
{
    char *split_cmnd;
    char send_buffer[1024]={'\0'};
    struct tm tm;
    time_t start_t, end_t;
    //Splitting based on space & newline
    split_cmnd = strtok(command, " \n");
    split_cmnd = strtok(NULL, " \n");


    if(split_cmnd != NULL)
    {
        if(!strcmp(split_cmnd,"--shortlist"))
        {
            split_cmnd = strtok(NULL, " \n");
            if(split_cmnd == NULL)
            {   
                //printf("Incorrect format used\n");
                sprintf(send_buffer, "Incorrect Format. The Correct Format is : IndexGet --shortlist <start_timestamp> <end_timestamp>.\n");
				send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
				bzero(send_buffer, 1024);
				return;
            }
            else
            {
                split_cmnd = strtok(NULL, " \n");
                if(strptime(split_cmnd, "%d-%m-%Y", &tm) != NULL)
                    start_t = mktime(&tm);
                else
                {
                	sprintf(send_buffer, "Incorrect Format. The Correct Format is : IndexGet --shortlist <start_timestamp> <end_timestamp>.\n");
					send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
					bzero(send_buffer, 1024);
				}
                split_cmnd = strtok(NULL, " \n");
                if(strptime(split_cmnd, "%d-%m-%Y", &tm) != NULL)
                    end_t = mktime(&tm);
                else
                {
                    sprintf(send_buffer, "Incorrect Format. The Correct Format is : IndexGet --shortlist <start_timestamp> <end_timestamp>.\n");
					send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
					bzero(send_buffer, 1024);
                }
                split_cmnd = strtok(NULL, " \n");
                if(split_cmnd != NULL)
                {
                    //printf("Incorrect format followed.\n");
                    sprintf(send_buffer, "Incorrect Format. The Correct Format is : IndexGet --shortlist <start_timestamp> <end_timestamp>.\n");
					send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
					bzero(send_buffer, 1024);
					return;
                }
            }
            //handleshortlist here
            DIR *dr;
            dr = opendir("./");
            struct dirent *drnt;
            struct stat filestat;
            off_t size;
            char filename[100];  
            time_t timestamp; 
            char type; 
            if(dr != NULL)
            {
                while(drnt = readdir(dr))
                {
                    if(stat(drnt->d_name, &filestat))
                        return;
                    else if(difftime(filestat.st_mtime,start_t) > 0 && difftime(end_t,filestat.st_mtime) > 0)
                    {
                        size = filestat.st_size;
                        timestamp = filestat.st_mtime;
                        //error will come here 
                        strcpy(filename,drnt->d_name);
                        type = (S_ISDIR(filestat.st_mode)) ? "d" : "-";
                    }
                }
                closedir(dr);
            }
            else
            {
                //printf("Directory could not be opened.\n");
                sprintf(send_buffer, "Directory cannot be opened.\n");
				send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
				bzero(send_buffer, 1024);
				return;
            }
        }
        else if(!strcmp(split_cmnd,"--longlist"))
        {
            //--longlist removed
            split_cmnd = strtok(NULL, " \n");
            if(split_cmnd == NULL)
            {
                DIR *dr;
                dr = opendir("./");
                struct dirent *drnt;
                struct stat filestat;
                off_t size;
                char filename[100];  
                time_t timestamp; 
                char type; 
                if(dr != NULL)
                {
                    while(drnt = readdir(dr))
                    {
                        if(stat(drnt->d_name, &filestat))
                            return;
                        else
                        {
                            size = filestat.st_size;
                            timestamp = filestat.st_mtime;
                            strcpy(filename,drnt->d_name);
                            type = (S_ISDIR(filestat.st_mode)) ? "d" : "-";
                            printf("%s %d\n",filename, size );
                        }
                    }
                    closedir(dr);
                }
                else
                {
                    //printf("Directory could not be opened.\n");
                    sprintf(send_buffer, "Directory cannot be opened.\n");
					send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
					bzero(send_buffer, 1024);
					return;
                }
            }
            else
            {
                //printf("Incorrect format used.\n");
                sprintf(send_buffer, "Incorrect Format. The Correct Format is : IndexGet --longlist.\n");
				send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
				bzero(send_buffer, 1024);
				return;
            }
        }
        else if(!strcmp(split_cmnd,"--regex"))
        {
            //--regex removed
            split_cmnd = strtok(NULL, " \n");
            if(split_cmnd != NULL)
            {
                //handleregexhere
                DIR *dr;
                struct dirent *drnt;
                dr = opendir ("./");
                struct stat filestat;
                struct stat buf;
                off_t size;
                char filename[100];  
                char type; 
                char regex_exp[100];
                strcpy(regex_exp,split_cmnd);
                FILE *pipe;
                char command[1024] = "ls ";
                char buffer[1024];
                char line[1024];

                if (dr != NULL)
                {
                    while (drnt = readdir(dr))
                    {
                        if(stat(drnt->d_name,&filestat) < 0)
                            return;
                        else
                        {
                            if((pipe = popen(command, "r")) == NULL)
                            {
                                //printf("error opening file\n");
                                sprintf(send_buffer, "Error opening file.\n");
								send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
								bzero(send_buffer, 1024);
								return;
                            }
                            while(fgets(buffer,1024,pipe))
                            {
                                strncpy(line,buffer,strlen(buffer)-1);
                                if(match(line,regex_exp) == 1)
                                {
                                    //printf("%s\n", line);
                                    stat(line, &buf);
                                    if(S_ISDIR(buf.st_mode) == 0) 
                                    {
                                        if(strcmp(line,drnt->d_name) == 0)
                                        {
                                            strcpy(filename,line);
                                            size = filestat.st_size;
                                            type = (S_ISDIR(filestat.st_mode)) ? 'd' : '-';
                                            printf("%s %d\n",filename, size );
                                            break;
                                        }
                                    }
                                }
                                bzero(line,sizeof(line));
                            }
                        }
                    }
                    pclose(pipe);
                }
                else
                {
                    //printf("Couldn't open the directory");
                    sprintf(send_buffer, "Directory cannot be opened.\n");
					send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
					bzero(send_buffer, 1024);
					return;
                }
            }
            else if(split_cmnd == NULL)
            {
                //printf("Incorrect format followed.\n");
                sprintf(send_buffer, "Incorrect Format. The Correct Format is : IndexGet --longlist.\n");
				send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
				bzero(send_buffer, 1024);
				return;
            }
            //regex argument removed
            split_cmnd = strtok(NULL, " \n");
            if(split_cmnd != NULL)
            {
                //printf("Incorrect format followed.\n");
                sprintf(send_buffer, "Incorrect Format. The Correct Format is : IndexGet --longlist.\n");
				send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
				bzero(send_buffer, 1024);
				return;
            }
        }
        else
        {
            //printf("Incorrect format used.\n");
            sprintf(send_buffer, "Incorrect format used.\n");
			send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
			bzero(send_buffer, 1024);
			return;
        }
    }
    else
    {
        //printf("Incorrect format used.\n");
        sprintf(send_buffer, "Incorrect format used.\n");
		send(connectionSocket,send_buffer,strlen(send_buffer)-1,0);
		bzero(send_buffer, 1024);
		return;
    }
}
